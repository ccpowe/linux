# Linux操作系统实验

1. 实验一：搭建Linux环境（Ubuntu20.04）
2. 实验二：上手Linux命令 + 搭建基于Java+MySQL的服务端应用
[参考1](https://blog.csdn.net/qq_16334741/article/details/108853138)  [参考二](https://blog.csdn.net/sinat_31350717/article/details/85002600)
3. 实验三：（1）配置vim+ctags+cscope+doxygen环境并使用以上环境阅读linux内核源码（推荐源代码版本为v2.6.0）[参考1](https://blog.csdn.net/jiaqi_ge/article/details/126951440)  [参考二](https://blog.csdn.net/andylauren/article/details/52830299) [参考三](https://blog.csdn.net/ReCclay/article/details/103538157)
（2）使用strace命令跟踪实验二中所用到的命令，查看每条命令都执行了哪些系统调用，将结果制成表格，格式如下表：

|命令|系统调用|
|-|-|
|ls| openat, newfstatat, getdents64, fstat, close, fstat, close|


4. 实验四：（1）与 open 这个系统调⽤相关的⽂件都有哪些，在每个⽂件⾥⾯都做了什么？请列表说明。（2）如果你要⾃⼰实现⼀个系统调⽤，能不能照着 open 来⼀个呢？（3）实现一个每天上午9:00自动清空回收站的系统调用，命名为：empty_trash()
5. 实验五：（1）掌握grub2的命令和配置，并通过它启动Ubuntu20.04 （2）实践项目 1：控制操作系统启动，具体请查看文档 实验实践项目
6. 实验六：（1）在源代码中查看task_struct的结构，找到任务的状态，在代码⾥⾯搜索⼀下这些状态改变的地⽅是哪个函数，是什么时机，从⽽进⼀步理解任务的概念。（2）⼀个进程的运⾏要保存很多信息，如下图所示，这些信息都可以通过命令⾏取出来，对于⼀个正在运⾏的进程，通过命令⾏找到上述进程运⾏的所有信息。
![task_struct](../images/task_struct.png)
7. 实验七： 通过 API 设置进程和线程的调度策略

